<?php

/**
 * @file
 * Describe hooks provided by the Crowd Provisioning module.
 */


/**
 * Implement custom logic, or alter data that will be used by Crowd
 * Providioning, before a new user is provisioned in crowd.
 *
 * @param array $form
 *   The Drupal registration form.
 * @param array $form_state
 *   The form state data representing submitted info for the registration form.
 * @param array $data
 *   An associative array of user data to be sent to Crowd when creating the new
 *   user remotely. Contains:
 *   - 'name': Username.
 *   - 'first-name': First name.
 *   - 'last-name': Last name.
 *   - 'email': email address
 *   - 'password': user password
 *   - 'display-name' (optional): Display name.
 *   - 'active' (optional): Whether to set user as active (true/false)
 */
function hook_crowd_provisioning_preprovision($form, $form_state, &$data) {
  // Set the first and last name values from custiom registration form fields.
  if (!empty($form_state['values']['field_first_name']) && !empty($form_state['values']['field_last_name'])) {
    // Set the first and last name info in the $data array.
    $data['first-name'] = $form_state['values']['field_first_name'][$form['field_first_name']['#language']][0]['value'];
    $data['last-name'] = $form_state['values']['field_last_name'][$form['field_last_name']['#language']][0]['value'];
  }
}


/**
 * Implement custiom logic after a new user has been created in both Drupal and
 * in Crowd.
 *
 * @param array $form
 *   The Drupal registration form.
 * @param array $form_state
 *   The form state data representing submitted info for the registration form.
 * @param object $account
 *   The newly created Drupal account object for the new user.
 */
function hook_crowd_provisioning_postprovision($form, $form_state, $account) {
  // If this user is registering from an internal IP, add them to a special
  // group in Crowd.
  if (strpos(ip_address(), '192.168') == 0) {
    // Connect to Crowd and set group.
    try {
      $crowd = crowd_client_connect();
      if ($crowd->addUserToGroup($account->name, 'internal-users')) {
        watchdog('mymodule', 'Added %user to group "internal-users" in Crowd.', array('%user' => $account->name), WATCHDOG_INFO);
      }
    }
    catch (CrowdException $e) {
      $e->logResponse();
    }
  }
}
